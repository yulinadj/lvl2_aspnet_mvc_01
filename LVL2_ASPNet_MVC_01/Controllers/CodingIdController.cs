﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_01.Controllers
{
    public class CodingIdController : Controller
    {
        // GET: CodingId
        [Route("CodingId")]
        [HttpGet]
        public ActionResult CodingIdMessage()
        {
            string html = "<form method= 'post' action='/CodingId/Display'>" +
                "<input type= 'text' name='name'/>" +
                "<input type= 'submit' value='Greet Me' />"+
                "</form>";
            return Content(html, "text/html");
        }
        [Route("CodingId")]
        [HttpPost]
        public ActionResult Display(string name = "World")
        {
            return Content(string.Format("<h1>Hello " + name + "</h1>"), "text/html");
        }

        [Route("CodingId/Hola")]

        public ActionResult Goodbye()
        {
            return View();
        }

    }
}